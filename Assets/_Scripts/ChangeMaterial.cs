﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour {

    public Material materialToChange;

	// Use this for initialization
	void Start ()
    {
		gameObject.GetComponent<Renderer>().material = materialToChange;

    }
}
